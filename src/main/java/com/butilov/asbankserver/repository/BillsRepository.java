package com.butilov.asbankserver.repository;

import com.butilov.asbankserver.entity.Bill;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;

@Repository
public interface BillsRepository extends CrudRepository<Bill, Long> {
    public @NotNull Bill findByName(String name);
}