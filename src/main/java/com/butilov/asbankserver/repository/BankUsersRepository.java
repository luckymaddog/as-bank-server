package com.butilov.asbankserver.repository;

import com.butilov.asbankserver.entity.BankUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;

@Repository
public interface BankUsersRepository extends CrudRepository<BankUser, Long> {
    @NotNull BankUser findByLogin(String login);
}