package com.butilov.asbankserver.repository;

import com.butilov.asbankserver.entity.Debet;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DebetsRepository extends CrudRepository<Debet, Long> {

}