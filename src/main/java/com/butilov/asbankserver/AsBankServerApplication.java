package com.butilov.asbankserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
public class AsBankServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AsBankServerApplication.class, args);
	}
}
