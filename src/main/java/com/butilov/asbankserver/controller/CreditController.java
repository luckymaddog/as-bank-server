package com.butilov.asbankserver.controller;

import com.butilov.asbankserver.entity.BankUser;
import com.butilov.asbankserver.entity.Credit;
import com.butilov.asbankserver.service.BankUserService;
import com.butilov.asbankserver.service.CreditService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;

@Slf4j
@RestController
@RequestMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
public class CreditController {

    private CreditService creditService;
    private BankUserService bankUserService;

    public CreditController(CreditService creditService, BankUserService bankUserService) {
        this.creditService = creditService;
        this.bankUserService = bankUserService;
    }

    @PostMapping(path = "credit")
    @ResponseBody
    public void createDebet(@RequestBody Credit credit) {
        BankUser foundUser = bankUserService.findByLogin(credit.getUser().getLogin());
        if (foundUser != null) {
            creditService.createCreditAndSave(credit, foundUser);
        } else {
            log.error("fail to create credit. user not found");
        }
    }

    // todo тестовый метод
    @GetMapping(path = "credit")
    public Credit getCredit() {
        Credit credit = new Credit();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 1);
        credit.setExpireTime(calendar.getTimeInMillis());

        credit.setRate((byte) 10);
        credit.setUser(new BankUser() {{
            setLogin("user1");
            setPassword("1");
        }});
        return credit;
    }
}