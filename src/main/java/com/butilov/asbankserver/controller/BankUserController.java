package com.butilov.asbankserver.controller;

import com.butilov.asbankserver.entity.BankUser;
import com.butilov.asbankserver.entity.Bill;
import com.butilov.asbankserver.service.BankUserService;
import com.butilov.asbankserver.service.BillService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.http.HTTPException;

@Slf4j
@RestController
@RequestMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
public class BankUserController {

    private BankUserService bankUserService;
    private BillService billService;

    public BankUserController(BankUserService bankUserService, BillService billService) {
        this.bankUserService = bankUserService;
        this.billService = billService;
    }

    @PostMapping(path = "registration")
    @ResponseBody
    public void registration(@RequestBody BankUser bankUser) {
        final BankUser foundBankUser = bankUserService.findByLogin(bankUser.getLogin());
        if (foundBankUser == null) {
            Bill bill = billService.createBillWithName(bankUser.getLogin());
            bankUser.setBill(bill);
            bankUserService.saveBankUser(bankUser);
        }
    }

    @GetMapping(path = "user/{i}")
    @ResponseBody
    public BankUser getUser(@PathVariable("i") String login) {
        return bankUserService.findByLogin(login);
    }


    @PostMapping(path = "login")
    @ResponseBody
    public BankUser login(@RequestBody BankUser bankUser) {
        final BankUser foundBankUser = bankUserService.findByLogin(bankUser.getLogin());
        if (foundBankUser == null) {
            log.info("bankUser " + bankUser.getLogin() + " not found");
            throw new HTTPException(HttpStatus.UNAUTHORIZED.value());
        } else {
            if (bankUserService.getPasswordHash(foundBankUser).equals(bankUser.getPassword())) {
                log.info("authentication success");
                return foundBankUser;
            } else {
                log.info("authentication error");
                throw new HTTPException(HttpStatus.FORBIDDEN.value());
            }
        }
    }
}