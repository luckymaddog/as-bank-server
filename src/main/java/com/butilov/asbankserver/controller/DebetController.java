package com.butilov.asbankserver.controller;

import com.butilov.asbankserver.entity.BankUser;
import com.butilov.asbankserver.entity.Bill;
import com.butilov.asbankserver.entity.Debet;
import com.butilov.asbankserver.service.BankUserService;
import com.butilov.asbankserver.service.DebetService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;

@Slf4j
@RestController
@RequestMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
public class DebetController {

    private DebetService debetService;
    private BankUserService bankUserService;

    public DebetController(DebetService debetService, BankUserService bankUserService) {
        this.debetService = debetService;
        this.bankUserService = bankUserService;
    }

    @PostMapping(path = "debet")
    @ResponseBody
    public void createDebet(@RequestBody Debet debet) {
        BankUser foundUser = bankUserService.findByLogin(debet.getUser().getLogin());
        if (foundUser != null) {
            Bill foundUserBill = foundUser.getBill();
            if (foundUserBill.getValue() >= debet.getValue()) {
                debetService.createDebetAndSave(debet, foundUser);
            } else {
                log.error("fail to create debet. user has no money");
            }
        } else {
            log.error("fail to create debet. user not found");
        }
    }

    // todo тестовый метод
    @GetMapping(path = "debet")
    public Debet getDebet() {
        Debet debet = new Debet();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 1);
        debet.setExpireTime(calendar.getTimeInMillis());

        debet.setRate((byte) 10);
        debet.setUser(new BankUser() {{
            setLogin("user1");
            setPassword("1");
        }});
        return debet;
    }
}