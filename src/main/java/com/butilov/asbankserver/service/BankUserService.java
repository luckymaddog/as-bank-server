package com.butilov.asbankserver.service;

import com.butilov.asbankserver.entity.BankUser;
import com.butilov.asbankserver.repository.BankUsersRepository;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

/**
 * Created by Dmitry Butilov
 * on 12.06.18.
 */
@Service
public class BankUserService {

    private BankUsersRepository bankUsersRepository;

    public BankUserService(BankUsersRepository bankUsersRepository) {
        this.bankUsersRepository = bankUsersRepository;
    }

    @NotNull
    public String getPasswordHash(BankUser bankUser) {
        return bankUser.getPassword();
    }


    public BankUser saveBankUser(BankUser bankUser) {
        bankUser.setPassword(getPasswordHash(bankUser));
        return bankUsersRepository.save(bankUser);
    }

    public BankUser findByLogin(String login) {
        return bankUsersRepository.findByLogin(login);
    }
}