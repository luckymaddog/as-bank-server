package com.butilov.asbankserver.service;

import com.butilov.asbankserver.entity.BankUser;
import com.butilov.asbankserver.entity.Bill;
import com.butilov.asbankserver.entity.Debet;
import com.butilov.asbankserver.entity.Transaction;
import com.butilov.asbankserver.repository.BankUsersRepository;
import com.butilov.asbankserver.repository.DebetsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Calendar;

/**
 * Created by Dmitry Butilov
 * on 12.06.18.
 */
@Slf4j
@Service
public class DebetService {

    private DebetsRepository debetsRepository;
    private BankUsersRepository bankUsersRepository;

    private BillService billService;
    private TransactionService transactionService;

    public DebetService(DebetsRepository debetsRepository,
                        BankUsersRepository bankUsersRepository,
                        BillService billService,
                        TransactionService transactionService) {
        this.debetsRepository = debetsRepository;
        this.bankUsersRepository = bankUsersRepository;
        this.billService = billService;
        this.transactionService = transactionService;
    }

    public void saveDebet(Debet debet) {
        debetsRepository.save(debet);
    }

    public void createDebetAndSave(Debet debet, BankUser bankUser) {
        String billName = bankUser.getLogin() + " debet" + (bankUser.getDebets().size() + 1);
        Bill debetBill = billService.createBillWithName(billName);
        debet.setBill(debetBill);
        debet.setExpireTime(getTimeForOneYearDebet());
        debet.setUser(bankUser);
        debetsRepository.save(debet);

        Transaction transaction = new Transaction();
        transaction.setUser(bankUser);
        transaction.setDestBill(debetBill);
        transaction.setValue(debet.getValue());
        transactionService.saveTransaction(transaction);

        bankUser.getDebets().add(debet);
        bankUsersRepository.save(bankUser);
    }

    private long getTimeForOneYearDebet() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 1);
        return calendar.getTimeInMillis();
    }
}